import React, { PureComponent } from 'react';


class About extends PureComponent {
	constructor(props) {
		super(props)
		// this.state = []
	}

	render() {
		return (

			<div id="colorlib-about">
				<div className="container">
					<div className="row text-center">
						<h2 className="bold">About</h2>
					</div>
					<div className="row row-padded-bottom">
						<div className="col-md-5 animate-box">
							<img className="img-responsive about-img" src="images/about.jpg" alt="html5 bootstrap template by colorlib.com" />
						</div>
						<div className="col-md-6 col-md-push-1 animate-box">
							<div className="about-desc">
								<h2><span>Dipesh</span><span>Sukhani</span></h2>
								<div className="desc">
									<div className="rotate">
										<h2 className="heading">About</h2>
									</div>
									<p>Presently: 
										<ul>
											<li>Writing Smart Contracts for <a href="https://zapper.fi" target="blank">Zapper</a></li>
											<li>Managing Suit of Smart Contracts for <a href="https://www.1x.exchange/" target="blank">1exchange Singapore</a> </li>
										</ul>
									</p>	
									<p>Solidity Developer, self-taught developer, CPA by qualification.</p>
									<br />
									<p>Dipesh left the shores of the corporate world to sail his yacht in the world of tech.  In 2017, Dipesh dove into the waters of blockchain and co-founded Indorse Pte Ltd.  Dipesh now focuses full-time on coding and making web apps.</p>
									<p className="colorlib-social-icons">
										{/* <a href="#"><i className="icon-facebook4"></i></a> */}
										<a href="https://twitter.com/amateurdev2"><i className="icon-twitter"></i></a>
										<a href="https://github.com/amateur-dev"><i className="icon-github"></i></a>
										<a href="https://www.linkedin.com/in/dipeshsukhani/"><i className="icon-linkedin"></i></a>
										<a href="https://dev.to/a_mature_dev"><i className="fab fa-dev"></i></a>
										<a href="mailto:me@dipeshsukhani.dev"><i class="fas fa-envelope-square"></i></a>
									</p>
									{/* <p><a href="work.html" className="btn btn-primary btn-outline">View My Works</a></p> */}
								</div>
							</div>
						</div>
					</div>
					{/* <div className="row">
						<div className="col-md-4 animate-box">
							<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						</div>
						<div className="col-md-4 animate-box">
							<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						</div>
						<div className="col-md-4 animate-box">
							<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						</div>
					</div> */}
				</div>
			</div >

		)
	}

}

export default About;


