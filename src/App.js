import React from 'react';
import logo from './logo.svg';
// import './App.css';
import About from './components/About'
import NavigationDrawer from './components/NavigationDrawer'
import Header from './components/Header'
import Services from './components/Services'
import Testimonials from './components/Services'
import Footer from './components/Footer'

function App() {
  return (
    <div id="colorlib-page">

      <Header />
      <About />
      {/* <Services /> */}

      {/* <Footer /> */}
    </div>
  );
}

export default App;
